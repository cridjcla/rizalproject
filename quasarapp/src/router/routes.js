const routes = [
  {
    path: '/',
    component: () => import('layouts/LandLayout.vue'),
    children: [
      { path: '', component: () => import('layouts/LandLayout.vue') },
      { path: 'home', component: () => import('pages/LandingPage.vue') },
      { path: 'earlyLife', component: () => import('pages/EarlyLifePage.vue') },
      { path: 'education', component: () => import('components/EducPage.vue') },
      { path: 'personalLife', component: () => import('components/PersonalLife.vue') },
      { path: 'pagbabalik', component: () => import('components/returnPage.vue') },
      { path: 'katha', component: () => import('components/PublishPage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
